Role Mount HDD
=========

Ansible role to format and 

Compatibility
------------

This role can be used only on Ubuntu 18. Other OS will come later

Variables Used
------------

You can find in defaults/main.yml all variables used in tasks

| Variable | Default Value | Type   | Description               |
| -------- | :------------ | :----- | ------------------------- |
| fstype   | ZFS          | String | Format type for the disk  |
| dev      | /dev/nvme0n1     | String | Partition to mount        |
| mount    | /home         | String | Path to mount             |
| opts     | rw            | String | Option for mount drive    |
| state    | mounted       | String | State for the mount drive |

## Author Information

Written by [Dimitri Colier](mailto:dimitri.colier@epfl.ch) for EPFL - STI school of engineering

Updated by [Pullyvan Krishnamoorthy](mailto:pullyvan.krishnamoorthy@epfl.ch) for EPFL - STI school of engineering